import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _db02fd54 = () => interopDefault(import('../pages/contact/index.vue' /* webpackChunkName: "pages/contact/index" */))
const _19925498 = () => interopDefault(import('../pages/informatie/index.vue' /* webpackChunkName: "pages/informatie/index" */))
const _106f6b2f = () => interopDefault(import('../pages/installations/index.vue' /* webpackChunkName: "pages/installations/index" */))
const _9bd736ca = () => interopDefault(import('../pages/over-ons/index.vue' /* webpackChunkName: "pages/over-ons/index" */))
const _d54d1212 = () => interopDefault(import('../pages/privacy-policy/index.vue' /* webpackChunkName: "pages/privacy-policy/index" */))
const _2d19f645 = () => interopDefault(import('../pages/rentals/index.vue' /* webpackChunkName: "pages/rentals/index" */))
const _dc4f8bfc = () => interopDefault(import('../pages/sales/index.vue' /* webpackChunkName: "pages/sales/index" */))
const _51dcade5 = () => interopDefault(import('../pages/terms-of-service/index.vue' /* webpackChunkName: "pages/terms-of-service/index" */))
const _8e6bf1ac = () => interopDefault(import('../pages/installations/_slug/index.vue' /* webpackChunkName: "pages/installations/_slug/index" */))
const _882d2c80 = () => interopDefault(import('../pages/rentals/_slug/index.vue' /* webpackChunkName: "pages/rentals/_slug/index" */))
const _64938b85 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))
const _22a3e200 = () => interopDefault(import('../pages/_slug/index.vue' /* webpackChunkName: "pages/_slug/index" */))
const _3838ca3b = () => interopDefault(import('../pages/_slug/_slug/index.vue' /* webpackChunkName: "pages/_slug/_slug/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/contact",
    component: _db02fd54,
    name: "contact"
  }, {
    path: "/informatie",
    component: _19925498,
    name: "informatie"
  }, {
    path: "/installations",
    component: _106f6b2f,
    name: "installations"
  }, {
    path: "/over-ons",
    component: _9bd736ca,
    name: "over-ons"
  }, {
    path: "/privacy-policy",
    component: _d54d1212,
    name: "privacy-policy"
  }, {
    path: "/rentals",
    component: _2d19f645,
    name: "rentals"
  }, {
    path: "/sales",
    component: _dc4f8bfc,
    name: "sales"
  }, {
    path: "/terms-of-service",
    component: _51dcade5,
    name: "terms-of-service"
  }, {
    path: "/installations/:slug",
    component: _8e6bf1ac,
    name: "installations-slug"
  }, {
    path: "/rentals/:slug",
    component: _882d2c80,
    name: "rentals-slug"
  }, {
    path: "/",
    component: _64938b85,
    name: "index"
  }, {
    path: "/:slug",
    component: _22a3e200,
    name: "slug"
  }, {
    path: "/:slug/:slug",
    component: _3838ca3b,
    name: "slug-slug"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config._app && config._app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}

/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import data from '@/static/data/installations.json'

export const state = () => ({
  installations: [],
})

export const mutations = {
  GET_INSTALL(state, installations) {
    state.installations = installations
  },
}

export const actions = {
  async getInstallations({ commit }) {
    try {
      const response = await data.installations.find((el) => {
        console.log(el)
        return el.slug === $nuxt.$route.params.slug
      })
      commit('GET_INSTALL', response)
    } catch (error) {
      console.log(error)
    }
  },
}

export const getters = {
  installations: (state) => {
    return state.installations
  },
}

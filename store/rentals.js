/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import data from '@/static/data/rentals.json'

export const state = () => ({
  rentals: [],
})

export const mutations = {
  GET_RENTALS(state, rentals) {
    state.rentals = rentals
  },
}

export const actions = {
  async getRentals({ commit }) {
    try {
      const response = await data.rentals
      commit('GET_RENTALS', response)
    } catch (error) {
      console.log(error)
    }
  },
  async getSingleRental({ commit }) {
    try {
      const response = await data.rentals.find((el) => {
        console.log($nuxt.$route.params.slug)
        return el.slug === $nuxt.$route.params.slug
      })
      commit('GET_RENTALS', response)
    } catch (error) {
      console.log(error)
    }
  },
}

export const getters = {
  rentals: (state) => {
    return state.rentals
  },
}

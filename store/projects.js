/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import data from '@/static/data/services.json'

export const state = () => ({
  projects: [],
})

export const mutations = {
  GET_PROJECTS(state, projects) {
    state.projects = projects
  },
}

export const actions = {
  async getProjects({ commit }) {
    try {
      const response = await data.services.find((el) => {
        if (el.slug === $nuxt.$route.params.slug) {
          return el.projects
        }
      })
      commit('GET_PROJECTS', response)
    } catch (error) {
      console.log(error)
    }
  },
  async getSingleProject({ commit }) {
    try {
      const response = await data.services.map((el) => {
        if (window.location.pathname.includes(el.slug)) {
          const getContent = el.projects.find((elem) => {
            console.log(elem)
            return elem.slug === $nuxt.$route.params.slug
          })
          commit('GET_PROJECTS', getContent)
        }
      })
    } catch (error) {
      console.log(error)
    }
  },
}

export const getters = {
  projects: (state) => {
    return state.projects
  },
}

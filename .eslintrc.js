module.exports = {
  root: true,
  env: {
    browser: true,
    node: true,
  },
  parserOptions: {
    parser: 'babel-eslint',
  },
  extends: [
    '@nuxtjs',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended',
  ],
  // required to lint *.vue files
  plugins: ['prettier'],
  // add your custom rules here
  rules: {
    semi: [2, 'never'],
    'no-console': 'off',
    'vue/max-attributes-per-line': 1,
    'prettier/prettier': ['error', { semi: false, endOfLine: 'auto' }],
    'vue/html-self-closing': 0,
    'vue/html-closing-bracket-newline': 'off',
    'vue/html-indent': 'off',
  },
}

import Vue from 'vue'
import * as VueGoogleMaps from 'vue2-google-maps'

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyDHK48b1RvkV68PG3ABSg1aF1Q9Xlj5BJg',
    libraries: 'places',
  },
})

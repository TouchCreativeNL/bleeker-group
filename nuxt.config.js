import rentals from './static/data/rentals.json'
import services from './static/data/services.json'
import installations from './static/data/installations.json'

const dynamicRoutes = () => {
  const routes = []

  function getRentals() {
    return new Promise((resolve) => {
      resolve(
        rentals.rentals.map((el) => {
          routes.push(`rentals/${el.slug}`)
        })
      )
    })
  }

  function getServices() {
    return new Promise((resolve) => {
      resolve(
        services.services.map((el) => {
          routes.push(`/${el.slug}`)
        })
      )
    })
  }

  function getFestivals() {
    return new Promise((resolve) => {
      resolve(
        services.services[0].projects.map((el) => {
          routes.push(`/${el.category}/${el.slug}`)
        })
      )
    })
  }

  function getConcerten() {
    return new Promise((resolve) => {
      resolve(
        services.services[1].projects.map((el) => {
          routes.push(`/${el.category}/${el.slug}`)
        })
      )
    })
  }

  function getBedrijfsfeesten() {
    return new Promise((resolve) => {
      resolve(
        services.services[2].projects.map((el) => {
          routes.push(`/${el.category}/${el.slug}`)
        })
      )
    })
  }

  function getCongressen() {
    return new Promise((resolve) => {
      resolve(
        services.services[3].projects.map((el) => {
          routes.push(`/${el.category}/${el.slug}`)
        })
      )
    })
  }

  function getOverige() {
    return new Promise((resolve) => {
      resolve(
        services.services[4].projects.map((el) => {
          routes.push(`/${el.category}/${el.slug}`)
        })
      )
    })
  }

  function getInstallations() {
    return new Promise((resolve) => {
      resolve(
        installations.installations.map((el) => {
          routes.push(`/${el.slug}`)
        })
      )
    })
  }

  getServices()
  getRentals()
  getInstallations()
  getFestivals()
  getConcerten()
  getBedrijfsfeesten()
  getCongressen()
  getOverige()

  return routes
}

export default {
  ssr: false,
  target: 'static',
  generate: {
    routes: dynamicRoutes,
  },
  router: {
    base: '/',
    scrollBehavior(to, from, savedPosition) {
      if (savedPosition) {
        return savedPosition
      }

      return { x: 0, y: 0 }
    },
  },
  /*
   ** Headers of the page
   */
  head: {
    title: 'Bleeker Group',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content:
          'Bleeker Group is uw audiovisuelepartner voor: Licht, geluid, video en rigging. Bel of mail voor een vrijblijvende offerte.',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    '@/assets/style/main.scss',
    '@/assets/fonts/harabara/harabara.scss',
    '@/assets/fonts/montserrat/montserrat.scss',
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/stylelint-module',
    [
      '@nuxtjs/google-analytics',
      {
        id: 'UA-68710882-1',
      },
    ],
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    'vue-scrollto/nuxt',
    [
      'nuxt-compress',
      {
        gzip: {
          cache: true,
        },
        brotli: {
          threshold: 10240,
        },
      },
    ],
  ],
  /*
   ** Build configuration
   */
  build: {
    analyze: false,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
}
